import controllers.DwellingController;

public class Main {

  public static void main(String[] args) {
    DwellingController dwellingController = new DwellingController();
    dwellingController.startApplication();
  }

}

package view;

public interface MenuOptions {

  String WELCOME = "Welcome in main menu.";
  String CHOOSE_ACTION = "Please, choose an action";
  String STARTUP_OPTIONS = "1 - show all items \n2 - sort by parameter \n3 - filter by "
      + "parameters";
  String SEARCH_IN_PARTICULAR_CLASS = "If you want to search in particular category enter Y, "
      + "if you want to work with all items enter N";
  String CATEGORY_DEFINING = "Please choose a category:\n 1 - private house\n 2 - high-rise "
      + "building";
  String CLASS_DEFINING = "Or a particular class: \n 3 - Mansion\n 4 - Townhouse\n 5 - Flat\n "
      + "6 - Penthouse";
  String NUMBER_OF_PARAMETERS = "Enter number of parameters";
  String CHOOSE_THE_PARAMETER = "Please, choose the parameter: ";
  String MAX_VALUE = "Pleas, enter maximal value";
  String MIN_VALUE = "Please, enter minimal value";
  String ASK_ABOUT_CONTINUE = "If you want To continue, enter Y, else enter N";

}

package models;

import java.util.ArrayList;
import java.util.List;

public class Realtor {

  private List<Dwelling> dwellings = new ArrayList<>();

  public Realtor() {
    fillTheDwellingsList();
  }


  public List<Dwelling> getDwellings() {
    return dwellings;
  }


  public void setDwellings(List<Dwelling> dwellings) {
    this.dwellings = dwellings;
  }

  private void fillTheDwellingsList() {
    this.dwellings.add(new Flat(23, 45, 5,
        8, 12, 54, FlatType.APARTMENT_STUDIO));
    this.dwellings.add(new Flat(28, 95, 78,
        1212, 1008, 13, FlatType.APARTMENT));
    this.dwellings.add(new Penthouse(12, 23, 34,
        67, 78, 24, 23));
    this.dwellings.add(new Penthouse(2, 203, 14,
        670, 1700, 204, 3));
    this.dwellings.add(new Mansion(34, 65, 98,
        23, 56, 12, 43));
    this.dwellings.add(new Mansion(54, 95, 18,
        2003, 560, 120, 430));
    this.dwellings.add(new Townhouse(12, 34, 56,
        78, 89, 56, 12, 23));
    this.dwellings.add(new Townhouse(2, 30, 6,
        780, 8900, 560, 120, 3));
  }
}

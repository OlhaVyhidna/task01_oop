package models;

public class Penthouse extends HousingInAHighRiceBuilding {

  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;
  private int floorNumber;

  public Penthouse(int numberOfFloorsInDwelling, int areaOfDwelling, int numberOfRooms,
      int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters, int floorNumber) {
    super(numberOfFloorsInDwelling, areaOfDwelling, numberOfRooms, distanceToKindergartenInMeters,
        distanceToSchoolInMeters, distanceToPlaygroundInMeters, floorNumber);
  }


  @Override
  public String toString() {
    return "Penthouse{" +
        "numberOfFloorsInDwelling=" + getNumberOfFloorsInDwelling() +
        ", areaOfDwelling=" + getAreaOfDwelling() +
        ", numberOfRooms=" + getNumberOfRooms() +
        ", distanceToKindergartenInMeters=" + getDistanceToKindergartenInMeters() +
        ", distanceToSchoolInMeters=" + getDistanceToSchoolInMeters() +
        ", distanceToPlaygroundInMeters=" + getDistanceToPlaygroundInMeters() +
        ", floorNumber=" + getFloorNumber() +
        '}';
  }
}

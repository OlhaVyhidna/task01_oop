package models;

public class Mansion extends PrivateHouse {

  private int numberOfFloorsInDwelling;
  private int areaOfDwelling;
  private int numberOfRooms;
  private int distanceToKindergartenInMeters;
  private int distanceToSchoolInMeters;
  private int distanceToPlaygroundInMeters;
  private int adjoiningTerritory;


  public Mansion(int numberOfFloorsInDwelling, int areaOfDwelling, int numberOfRooms,
      int distanceToKindergartenInMeters, int distanceToSchoolInMeters,
      int distanceToPlaygroundInMeters, int adjoiningTerritory) {
    super(numberOfFloorsInDwelling, areaOfDwelling, numberOfRooms, distanceToKindergartenInMeters,
        distanceToSchoolInMeters, distanceToPlaygroundInMeters, adjoiningTerritory);
  }


  @Override
  public String toString() {
    return "Mansion{" +
        "numberOfFloorsInDwelling=" + getNumberOfFloorsInDwelling() +
        ", areaOfDwelling=" + getAreaOfDwelling() +
        ", numberOfRooms=" + getNumberOfRooms() +
        ", distanceToKindergartenInMeters=" + getDistanceToKindergartenInMeters() +
        ", distanceToSchoolInMeters=" + getDistanceToSchoolInMeters() +
        ", distanceToPlaygroundInMeters=" + getDistanceToPlaygroundInMeters() +
        ", adjoiningTerritory=" + getAdjoiningTerritory() +
        '}';
  }
}
